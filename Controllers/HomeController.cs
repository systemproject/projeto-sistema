﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sistema.Models;
using Microsoft.Data.SqlClient;
using System.Data;

namespace Sistema.Controllers
{
    public class PersonModel
    {
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Endereco { get; set; }
        public string Telefone { get; set; }
    }

    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public string DBConnection() {
            var datasource = @"localhost,1433"; 
            var database = "Sistema"; 
            var username = "SA"; 
            var password = "997851Gabriel!"; 
            var trustservercertificate = true;

            string connString = $@"Data Source={datasource};
                                   Initial Catalog={database};
                                   User ID={username};
                                   Password={password};
                                   TrustServerCertificate={trustservercertificate}";
            return connString;            
        }

        public string getRandomUser()
        {
            try 
            { 
                using (SqlConnection connection = new SqlConnection(DBConnection()))
                {
                    connection.Open();       

                    String sql = "SELECT * FROM Usuario ORDER BY NEWID()";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var codigo = reader.GetString(0);
                                var nome = reader.GetString(1);
                                var cpf = reader.GetString(2);
                                var endereco = reader.GetString(3);
                                var telefone = reader.GetString(4);
                                return $"{codigo}, {nome}, {cpf}, {endereco}, {telefone}";
                            }
                        }
                    }                    
                }
                return "";
            }
            catch (SqlException e)
            {
                return e.ToString();
            }            
        }

        public List<PersonModel> getAllUsers()
        {
            var users = new List<PersonModel>();
            using (SqlConnection connection = new SqlConnection(DBConnection()))
            {
                connection.Open();       

                String sql = "SELECT * FROM Usuario";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var codigo = reader.GetString(0);
                            var nome = reader.GetString(1);
                            var cpf = reader.GetString(2);
                            var endereco = reader.GetString(3);
                            var telefone = reader.GetString(4);
                            users.Add(new PersonModel() {
                                Codigo = codigo,
                                Nome = nome,
                                CPF = cpf,
                                Endereco = endereco,
                                Telefone = telefone,
                            });
                        }
                    }
                }                    
            }
            return users;    
        }

        public string deleteUser(string codigo)
        {
            using (SqlConnection connection = new SqlConnection(DBConnection()))
            {
                connection.Open();       

                String sql = "Delete From Usuario Where id = @codigo";
                try 
                {
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.Add("@codigo", SqlDbType.VarChar).Value = codigo;
                        command.ExecuteNonQuery();    
                    }     
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            } 
            return codigo;
        }

        public string updateUser(PersonModel person)
        {
            using (SqlConnection connection = new SqlConnection(DBConnection()))
            {
                connection.Open();       

                String sql = "UPDATE Usuario SET nome = @nome, cpf = @cpf, endereco = @endereco, telefone = @telefone Where id = @codigo";
                try 
                {
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@codigo", person.Codigo);
                        if (person.Nome == null) {
                            command.Parameters.AddWithValue("@nome", "");
                        } else {
                            command.Parameters.AddWithValue("@nome", person.Nome);
                        }
                        if (person.CPF == null) {
                            command.Parameters.AddWithValue("@cpf", "");
                        } else {
                            command.Parameters.AddWithValue("@cpf", person.CPF);
                        }
                        if (person.Endereco == null) {
                            command.Parameters.AddWithValue("@endereco", "");
                        } else {
                            command.Parameters.AddWithValue("@endereco", person.Endereco);                            
                        }
                        if (person.Telefone == null) {
                            command.Parameters.AddWithValue("@telefone", "");
                        } else {
                            command.Parameters.AddWithValue("@telefone", person.Telefone);     
                        }
                        command.ExecuteNonQuery();    
                    }     
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            } 
            return person.Codigo;
        }

        public void insertUser(PersonModel person)
        {
            using (SqlConnection connection = new SqlConnection(DBConnection()))
            {
                connection.Open();       

                String sql = "INSERT INTO Usuario VALUES (@codigo, @nome, @cpf, @endereco, @telefone);";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.Parameters.Add("@codigo", SqlDbType.VarChar, 20).Value = person.Codigo;  
                    command.Parameters.Add("@nome", SqlDbType.VarChar, 50).Value = person.Nome;
                    command.Parameters.Add("@cpf", SqlDbType.VarChar, 14).Value = person.CPF;
                    if (person.Endereco == null) {
                        command.Parameters.Add("@endereco", SqlDbType.VarChar, 80).Value = "";
                    } else {
                        command.Parameters.Add("@endereco", SqlDbType.VarChar, 80).Value = person.Endereco;
                    }
                    if (person.Telefone == null) {
                        command.Parameters.Add("@telefone", SqlDbType.VarChar, 20).Value = "";
                    } else {
                        command.Parameters.Add("@telefone", SqlDbType.VarChar, 20).Value = person.Telefone;
                    }
                    command.ExecuteNonQuery();                     
                }                    
            } 
        }

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Details()
        {
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }

        public IActionResult Users()
        {
            ViewBag.Usuarios = getAllUsers();
            return View();
        }

        [HttpGet]
        public ActionResult RandomUser()
        {
            return Json(getRandomUser());
        }

        [HttpDelete]
        public ActionResult Delete(string codigo)
        {
            return Json(deleteUser(codigo));
        }

        [HttpPost]
        public ActionResult Update(PersonModel person)
        {       
            try 
            {
                updateUser(person);
            }
            catch (SqlException e)
            {
                return Json(e.ToString());
            }   
            return Json($"Usuário atualizado com sucesso!");
        }

        [HttpGet]
        public ActionResult AllUsers()
        {
            return Json(getAllUsers());
        }

        [HttpPost]
        public ActionResult RegisterNewUser(PersonModel person)
        {
            PersonModel pessoa = new PersonModel
            {
                Codigo =  person.Codigo,
                Nome = person.Nome,
                CPF = person.CPF,
                Endereco = person.Endereco,
                Telefone = person.Telefone
            };
            try 
            {
                insertUser(pessoa);
            }
            catch (SqlException)
            {
                return Json("CPF informado já existe!");
            }     
            return Json(
                $"Usuário registrado com sucesso! Código: {pessoa.Codigo}, CPF: {pessoa.CPF.Substring(0,5)}"
            );
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
