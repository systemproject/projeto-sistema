﻿var cpf = document.getElementById("cpf");
if (cpf) {
  Inputmask({ regex: "\\d\\d\\d\\.\\d\\d\\d\\.\\d\\d\\d-\\d\\d" }).mask(cpf); 
}

var telefone = document.getElementById("telefone");
if (telefone) {
  Inputmask({ regex: "\\(\\d\\d\\) \\d\\d\\d\\d\\d-\\d\\d\\d\\d" }).mask(telefone);   
}

var cpfUpdate = document.getElementById("cpf-update");
if (cpfUpdate) {
  Inputmask({ regex: "\\d\\d\\d\\.\\d\\d\\d\\.\\d\\d\\d-\\d\\d" }).mask(cpfUpdate); 
}

var telefoneUpdate = document.getElementById("telefone-update");
if (telefoneUpdate) {
  Inputmask({ regex: "\\(\\d\\d\\) \\d\\d\\d\\d\\d-\\d\\d\\d\\d" }).mask(telefoneUpdate);   
}

var codigo = document.getElementById("codigo");
if (codigo) {
  codigo.value = makeid(20);  
}