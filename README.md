# Projeto Sistema

Projeto para Avaliação de Processo Seletivo Desenvolvedor Web.

**Vídeo Demonstrativo:** https://youtu.be/jI-r_OKkX0E

## Tecnologias

Foram utilizadas as seguintes tecnologias:

- C#
- AngularJS 1.7.8
- Bootstrap
- jQuery
- Microsoft SQL Server 2019 (RTM-CU17)

## Banco de Dados

Foi criado apenas uma tabela chamada de **Usuario** com o seguinte script:

```sql
CREATE TABLE Usuario (
    id NVARCHAR(20) UNIQUE NOT NULL, 
    nome NVARCHAR(50) NOT NULL, 
    cpf NVARCHAR(14) UNIQUE NOT NULL, 
    endereco NVARCHAR(80), 
    telefone NVARCHAR(20)
)
```

**Observação:** O projeto foi desenvolvido em uma máquina Linux, o banco de dados SQL Server foi executado através do Docker. Caso seja necessário, neste [Link](https://github.com/the-akira/ExemplosCSharp/blob/master/Code/DBConnection/Configura%C3%A7%C3%A3o.md) há instruções para instalar e configurar o banco de dados SQL Server no Linux.

## Execução

Ao executar o projeto com o comando `dotnet run`, é possível acessá-lo no browser através do seguinte endereço: `https://localhost:8001/`.

## API Endpoints

É possível interagir com a aplicação através de requisições HTTP à endpoints específicos. 

Os exemplos a seguir mostram a comunicação sendo realizada com a ferramente [httpie](https://httpie.io/):

#### Obtendo uma lista de todos os usuários

```
http --verify=no https://localhost:8001/Home/AllUsers
```

#### Obtendo um usuário aleatório

```
http --verify=no https://localhost:8001/Home/RandomUser
```

#### Registrando um novo usuário

```
http --verify=no --form post https://localhost:8001/Home/RegisterNewUser \
 codigo="Sd0aGLI1euQWVN86b0RK" \
 nome="augusto" \
 cpf="555.343.344-44" \
 endereco="Rua X" \
 telefone="48951478877"
```

#### Atualizando um usuário

```
http --verify=no --form post https://localhost:8001/Home/Update \
 codigo="Sd0aGLI1euQWVN86b0RK" \
 nome="luiz" \
 cpf="555.343.344-44" \
 endereco="Rua Y" \
 telefone="48951478877"
```

#### Removendo um usuário

```
http --verify=no --form delete https://localhost:8001/Home/Delete \
 codigo="Sd0aGLI1euQWVN86b0RK"
```